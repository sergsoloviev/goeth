package main

// os x ethernet switch

import (
	"errors"
	"fmt"
	"log"
	"net"
	"os/exec"
)

type NetworkConfiguration struct {
	Name   string
	Ip     string
	Mask   string
	Router string
}

var (
	work = NetworkConfiguration{
		Name:   "work",
		Ip:     "192.168.1.21",
		Mask:   "255.255.255.0",
		Router: "192.168.1.1",
	}
	home = NetworkConfiguration{
		Name:   "home",
		Ip:     "192.168.10.50",
		Mask:   "255.255.255.0",
		Router: "192.168.10.3",
	}

	configurations = []NetworkConfiguration{
		home,
		work,
	}
)

func main() {
	ip, err := get_ip()
	if err != nil {
		panic(err)
	}
	configuration0, err := check_ip(ip)
	if configuration0 == work {
		switch_configuration(home)
	} else if configuration0 == home {
		switch_configuration(work)
	}
}

func switch_configuration(conf NetworkConfiguration) error {
	fmt.Printf("try to switch configuration to %s, %s\n", conf.Name, conf.Ip)
	_, err := exec.Command("networksetup", "-setmanual", "Ethernet", conf.Ip, conf.Mask, conf.Router).Output()
	if err != nil {
		log.Fatal(err)
	}
	return nil
}

func check_ip(Address string) (NetworkConfiguration, error) {
	for _, e := range configurations {
		if e.Ip == Address {
			fmt.Printf("ok, we are at %s configuration now\n", e.Name)
			return e, nil
		}
	}
	return NetworkConfiguration{}, errors.New("can't determine configuration")
}

func get_ip() (string, error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "", err
	}
	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String(), nil
			}
		}
	}
	return "", errors.New("no ip detected")
}
